import voices from '../model/voices.js'

Vue.component('v-select', VueSelect.VueSelect)

const app = new Vue({
  el: '#app',
  data: {
    voices,
    voicesBackup: voices,
    favourites: [],
    search: '',
    categoryFilter: [],
    sortFilter: [
      {
        title: 'ascendente',
        value: 'asc'
      },
      {
        title: 'descendente',
        value: 'desc'
      }
    ],
    selectCategory: '',
    selectSort: ''
  },
  mounted: function() {
  },
  created: function () {
    this.selectAllFavourites()
    this.verifySelectVoices()
    this.createCategoryList()
  },
  watch: {
    search: function (val) {
      this.searchVoices(val)
      if(val !== null) {
        document.querySelector('.input-search .close').style.display = 'flex'
      } else {
        document.querySelector('.input-search .close').style.display = 'none'
      }
    },
    selectCategory: function(val) {
      if(val !== null) {
        this.filterCategoryVoices(val)
      } else {
        this.voices = this.voicesBackup
      }
    },
    selectSort: function(val) {
      this.sortVoices(val)
    }
  },
  methods: {
    addFavouriteList: function(voice) {
      let checkVoice = this.favourites.find(favourite => favourite.id == voice.id)
      if(checkVoice === undefined) {
        this.favourites.push(voice)
        localStorage.setItem('favourites', JSON.stringify(this.favourites))
        this.verifySelectVoices()
      } else {
        this.favourites.forEach((item, index) => {
          if (item.id == voice.id) {
            this.favourites.splice(index, 1);
            localStorage.setItem('favourites', JSON.stringify(this.favourites))
            this.verifySelectVoices()
          }
        })
      }
    },
    selectAllFavourites: function() {
      this.favourites = localStorage.getItem('favourites') !== null ? JSON.parse(localStorage.getItem('favourites')) : []
    },
    verifySelectVoices: function() {
      voices.forEach((voice, index) => {
        if(this.favourites.length === 0) voices[index]['select'] = false;
        this.favourites.find(favourite => favourite.id == voice.id ? voices[index]['select'] = true : voices[index]['select'] = false)
      })
    },
    searchVoices: function(filter) {
      this.voices = filter === '' ? this.voicesBackup : this.voicesBackup.filter((value) => value.name.toLowerCase().indexOf(filter.toLowerCase()) > -1)
    },
    resetInputSearch: function() {
      this.search = ''
      this.voices = this.voicesBackup
    },
    createCategoryList: function() {
      voices.forEach((voice, index) => {
        let checkCategory = this.categoryFilter.find(category => category == voice['tags'][0])
        if(checkCategory === undefined) {
          this.categoryFilter.push(voice.tags[0])
        }
      })
    },
    filterCategoryVoices: function(filter) {
      this.voices = filter === '' ? this.voicesBackup : this.voicesBackup.filter((value) => value.tags[0].toLowerCase().indexOf(filter.toLowerCase()) > -1)
    },
    sortVoices: function(filter) {
      if(filter.value === 'asc') {
        this.voices = this.voices.sort(function(a, b) {
          return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
        });
      } else {
        this.voices = this.voices.sort(function(a, b) {
          return a.name > b.name ? -1 : a.name < b.name ? 1 : 0;
        });
      }
    },
    selectRandomVoice: function() {
      this.addFavouriteList(this.voices[Math.floor(Math.random() * this.voices.length)]);
    }
  }
});